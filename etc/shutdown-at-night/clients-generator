#!/usr/bin/perl

use strict;
use warnings;

use Socket;
use SiteSummary;

use vars qw(%hostmap);

$ENV{PATH} .= ':/usr/sbin';

my @host_whitelist = `netgroup -h shutdown-at-night-hosts 2>/dev/null || true`;
my @host_blacklist = `netgroup -h shutdown-at-night-hosts-blacklist 2>/dev/null || true`;

my @wakeup_host_whitelist = `netgroup -h shutdown-at-night-wakeup-hosts 2>/dev/null || printf "NETGROUP-NOT-FOUND"`;
if(((exists $wakeup_host_whitelist[0]) && ($wakeup_host_whitelist[0] ne "NETGROUP-NOT-FOUND")) || (scalar(@wakeup_host_whitelist) == 0)) {
    @host_whitelist = @wakeup_host_whitelist;
    # when wakeup-in-the-morning-hosts netgroup is used, expect its own blacklist pendant (i.e., netgroup no-wakeup-in-the-morning-hosts)
    @host_blacklist = ();
}

# only use the no-wakeup-in-the-morning
my @wakeup_host_blacklist = `netgroup -h shutdown-at-night-wakeup-hosts-blacklist 2>/dev/null || printf "NETGROUP-NOT-FOUND"`;
if(((exists $wakeup_host_blacklist[0]) && ($wakeup_host_blacklist[0] ne "NETGROUP-NOT-FOUND")) || (scalar(@wakeup_host_blacklist) == 0)) {
    @host_blacklist = @wakeup_host_blacklist;
}

chomp @host_whitelist;
chomp @host_blacklist;
map { $hostmap{$_} = 1; } @host_whitelist;
foreach my $blacklisted_host (@host_blacklist) {
    if($blacklisted_host) { $hostmap{$blacklisted_host} = 0; }
}
# comment out for better debugging...
#print STDERR "$_ $hostmap{$_}\n" for (keys %hostmap);

for_all_hosts(\&host_handler);
exit(0);

sub host_handler {
    my $hostid = shift;
    my $ipaddr = SiteSummary::get_primary_ip_address($hostid);
    my $fqdn = scalar gethostbyaddr(inet_aton($ipaddr), AF_INET);
    if ($fqdn) {
        my $mac = (get_ether_hwaddr($hostid))[0];
        if ((exists $hostmap{$fqdn}) && ($hostmap{$fqdn} == 1)) {
            print "$fqdn $mac\n";
        }
    }
}

sub get_ether_hwaddr {
    my $hostid = shift;
    my $path = get_filepath_current($hostid, "/system/ifconfig-a");
    if (open(FILE, "<", $path)) {
        my $sysinfo = 0;
        my @hwaddr = ();
        while (<FILE>) {
            chomp;
            if (m/ether (([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2}))\s+.*$/) {
                push(@hwaddr, $1);
            }
            elsif (m/Link encap:Ethernet  HWaddr (.+\S)\s*$/) {
                push(@hwaddr, $1);
            }
        }
        close(FILE);
        return @hwaddr;
    } else {
        return undef;
    }
}
