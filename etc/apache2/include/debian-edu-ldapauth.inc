AuthType                        Basic
AuthBasicProvider               ldap file
AuthName                        "Administrative authentication needed to access this URL"
AuthUserFile                    /dev/null
AuthBasicAuthoritative          On
AuthLDAPURL                     ldaps://ldap.intern/dc=skole,dc=skolelinux,dc=no?uid?sub?(objectClass=posixAccount)
AuthLDAPGroupAttribute          memberUid
AuthLDAPGroupAttributeIsDN      Off

# set LogLevel to debug in VirtualHost's config section for debugging LDAP authentication/authorization
