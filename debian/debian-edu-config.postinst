#!/bin/sh

set -e

running_from_di() {
    # Look for file created by base-installer and removed at the end
    # of the d-i run.
    [ -e /etc/apt/apt.conf.d/00IgnoreTimeConflict ]
}

# This block must be done on upgrades before debconf is enabled, and
# not when the upgrade is done from within d-i.
if [ ! "$DEBIAN_HAS_FRONTEND" ] && [ "$1" = configure ] && [ -n "$2" ] && \
    ! running_from_di ; then
    # Purge clear text passwords left over from the installation from
    # debconf templates.dat and templates.dat-old (BTS #711251).
    # Calling debconf-set-selections twice to also purge it from
    # templates.dat-old.
    clearpwd=false
    for template in \
	debian-edu-config/kdc-password \
	debian-edu-config/kdc-password-again \
	debian-edu-config/ldap-password-again \
	debian-edu-config/ldap-password \
	debian-edu-config/first-user-password ; do
	if [ -n "$(perl -MDebconf::Db -MDebconf::Template -e "Debconf::Db->load; my \$template = Debconf::Template->get('$template'); print \$template->default || \$template->value;")" ]; then
	    clearpwd=true
	fi
    done
    wipepwdsfromdebconf() {
	cat <<EOF | debconf-set-selections
debian-edu-config debian-edu-config/kdc-password password
debian-edu-config debian-edu-config/kdc-password-again password
debian-edu-config debian-edu-config/ldap-password password
debian-edu-config debian-edu-config/ldap-password-again password
debian-edu-config debian-edu-config/first-user-password password
EOF
    }
    if $clearpwd ; then
	echo "info: Found clear text passwords in debconf database.  Wiping them."
	wipepwdsfromdebconf
	wipepwdsfromdebconf
    fi
fi

# Enable debconf
. /usr/share/debconf/confmodule

# Execute early, to get the state before changes activated by this
# package is done.
if command -v etckeeper > /dev/null ; then
    etckeeper commit "start of debian-edu-config postinst" || true
fi

# Update config for init.d/update-hostname based on debconf preseeding
ENABLED_DEFAULT=false
ENABLED=$ENABLED_DEFAULT
DEFAULTFILE=/etc/default/update-hostname
# Load current value if set in /etc/
if [ -f $DEFAULTFILE ] ; then
    . $DEFAULTFILE
    if [ "$ENABLED" ] ; then
	db_set debian-edu-config/update-hostname "$ENABLED"
    fi
fi
db_get debian-edu-config/update-hostname
# Only replace if the value changed
if [ -f $DEFAULTFILE ] ; then
    if [ "$RET" != "$ENABLED" ] ; then
	sed "s/ENABLED=.+/ENABLED=\"$RET\"" < $DEFAULTFILE > $DEFAULTFILE.new &&
	    mv $DEFAULTFILE.new $DEFAULTFILE	    
    fi
else
    # Only create or update the file if the value isn't the default value
    if [ "$ENABLED_DEFAULT" != "$RET" ] ; then
	echo "ENABLED=\"$RET\"" >> $DEFAULTFILE
    fi
fi

db_get debian-edu-config/enable-nat
FILE="/etc/default/enable-nat"
# Check the value for enable-nat and set it according to the boolean
if [ "$RET" = "false" ] ; then
	if [ ! -e "$FILE" ] ; then
		echo "NETWORK_TO_NAT=" > $FILE
	fi
fi

pam-auth-update --package

# Some init-scripts fail if to many fd is open ??
# close debconf db_handle before they start
db_stop


# start the enable-nat init script if we have a ltspserver
if [ -f /etc/debian-edu/config ] && grep -Eq "(LTSP-Server|Thin-Client-Server)" /etc/debian-edu/config ; then
	if ! grep -q Main-Server /etc/debian-edu/config ; then
		if command -v invoke-rc.d >/dev/null; then
			invoke-rc.d enable-nat start || exit $?
		else
			/etc/init.d/enable-nat start || exit $?
		fi
	fi
fi

case "$1" in
configure)
    # sssd refuses to read the file if it has any other mode
    chmod 600 /etc/sssd/sssd-debian-edu.conf
    chown root:root /etc/sssd/sssd-debian-edu.conf

    # Add post-up and update-hostname stanzas to interfaces file to let proxy
    # changes take effect immediately after reboot (would take up to 15 minutes
    # otherwise). Exclude a plain main server and the gateway; the script
    # doesn't make sense in both cases and would taint network setup.
	if [ -s /etc/debian-edu/config ] && ! grep -Eq "(Main-Server)" /etc/debian-edu/config ; then
		rm -f /etc/network/if-up.d/wpad-proxy-update
		rm -f /etc/network/if-up.d/hostname
		if [ -f /etc/network/interfaces ] && ! grep -q gateway /etc/hostname ; then
			if ! grep -q update-proxy /etc/network/interfaces ; then
				sed -i '/iface eth0 inet dhcp/a \    post-up \/usr\/share\/debian-edu-config\/tools\/update-proxy-from-wpad' \
				/etc/network/interfaces
			fi
			if ! grep -q update-hostname /etc/network/interfaces ; then
				sed -i '/iface eth0 inet dhcp/a \    post-up \/usr\/sbin\/update-hostname-from-ip' \
				/etc/network/interfaces
			fi
		fi
	fi

	# On Debian Edu main servers create a debian-edu system user account with
	# limited privileges for publishing host keytabs to diskless workstations (this
	# is the initial use case, further use cases might pop up later).
	if [ -s /etc/debian-edu/config ] && grep -Eq "(Main-Server)" /etc/debian-edu/config ; then

	    if ! getent 'passwd' 'debian-edu' >'/dev/null'; then
		echo 'Creating debian-edu user.' >&2
		adduser --system --home /var/lib/debian-edu \
		        --disabled-password --shell /bin/sh \
		        --group debian-edu
	    else
		echo 'User debian-edu already exists.' >&2
		# make sure all settings are appropriate
		if [ "$(id -gn 'debian-edu')" != 'debian-edu' ]; then
		    usermod --gid 'debian-edu' 'debian-edu'
		fi
	    fi

	    # Assure that permissions of /var/lib/debian-edu/ are appropriate
	    if [ -d /var/lib/debian-edu/ ]; then
		chown debian-edu:debian-edu /var/lib/debian-edu/
		chmod 0755 /var/lib/debian-edu/
	    fi

	fi

    # silence dovecot's message: if you have trouble with authentication failures,
    # enable auth_debug setting. See http://wiki.dovecot.org/WhyDoesItNotWork
    # This message goes away after the first successful login.
    mkdir -p /var/lib/dovecot
    chmod 750 /var/lib/dovecot
    chown root:root /var/lib/dovecot
    touch /var/lib/dovecot/auth_success
    if [ ! -d /var/lib/cfengine3/inputs/debian-edu ] ; then
        . /usr/share/debian-edu-config/tools/setup-cfengine3
    else
        cp /etc/cfengine3/debian-edu/cf.* /var/lib/cfengine3/inputs/debian-edu
        cp /etc/cfengine3/debian-edu/edu.cf /var/lib/cfengine3/inputs/debian-edu
        cp /etc/cfengine3/debian-edu/promises.cf /var/lib/cfengine3/inputs
    fi

	# Move LDAP BDB data base to default MDB one.
	if dpkg --compare-versions "$2" le "2.12.1" && grep -q Main-Server /etc/debian-edu/config && \
		[ ! -f /var/lib/ldap/data.mdb ] && ! running_from_di ; then
		TMPDIR=$(mktemp -d)
		slapcat > $TMPDIR/all.ldif
		invoke-rc.d slapd stop
		rm /var/lib/ldap/*
		ln -sf /etc/ldap/slapd-debian-edu-mdb.conf /etc/ldap/slapd.conf
		invoke-rc.d slapd start
		slapadd -l $TMPDIR/all.ldif
		if [ -f /var/lib/ldap/data.mdb ] ; then
			rm $TMPDIR/all.ldif
			rm -f /etc/ldap/slapd-debian-edu.conf
		fi
	fi
	# Remove obsolete conffile.
	if dpkg --compare-versions "$2" le "2.12.5" && [ -f /var/lib/ldap/data.mdb ] ; then
		rm -f /etc/ldap/slapd-debian-edu.conf
	fi
	# Cleanup from smbldap-tools and ldapscripts related files.
	if dpkg --compare-versions "$2" le "2.12.6" ; then
		rm -f /etc/ldapscripts/*.cf-before-edit
		rm -f /etc/smbldap-tools/*
		if [ -d /etc/smbldap-tools ] ; then
			rmdir /etc/smbldap-tools
		fi
	fi
	# Unregister init script fetch-ldap-cert
	if dpkg --compare-versions "$2" le "2.12.33"; then
	    update-rc.d -f fetch-ldap-cert remove
	fi

	# Update dconf databases
	command -v dconf >/dev/null && dconf update
    ;;
esac

#DEBHELPER#

# On the main-server, point from the old keytab location /etc/debian-edu/host-keytabs to the new
# keytab location at /var/lib/debian-edu/host-keytabs...
if grep -q Main-Server /etc/debian-edu/config; then
	if [ ! -e /etc/debian-edu/host-keytabs ] && [ -d /var/lib/debian-edu/host-keytabs ]; then
		ln -s /var/lib/debian-edu/host-keytabs /etc/debian-edu/host-keytabs
	fi
fi

# Register all changes done by this postinst script
if command -v etckeeper > /dev/null ; then
    etckeeper commit "end of debian-edu-config postinst" || true
fi

exit 0
