http_access allow localnet

# Cache larger files to cache more debian packages
maximum_object_size 153600 KB

#  - Appends .intern to hostnames without any dots in them.
append_domain .intern

# Currently, Debian Edu does not support IPv6 on the internal network
# thus, we should try to use DNSv4 preferrably for the http proxy.
# See https://bugs.debian.org/1006375
dns_v4_first on

# Adjust cache size to fit size of /var/spool/squid, the initial capacity value
# is dynamically updated using
# /usr/share/debian-edu-config/tools/squid-update-cachedir
cache_dir ufs /var/spool/squid 100 16 256

# FIXME: the next settings can be removed once #913950 (src:squid) is fixed.
# Begin: Improve handling of Debian packages (taken from squid-deb-proxy)
# we need a big cache, some debs are huge
maximum_object_size 512 MB

# tweaks to speed things up
cache_mem 200 MB
maximum_object_size_in_memory 10240 KB

# refresh pattern for debs and udebs
refresh_pattern deb$   129600 100% 129600
refresh_pattern udeb$   129600 100% 129600
refresh_pattern tar.gz$  129600 100% 129600
refresh_pattern tar.xz$  129600 100% 129600
refresh_pattern tar.bz2$  129600 100% 129600

# always refresh Packages and Release files
refresh_pattern \/(Packages|Sources)(|\.bz2|\.gz|\.xz)$ 0 0% 0 refresh-ims
refresh_pattern \/Release(|\.gpg)$ 0 0% 0 refresh-ims
refresh_pattern \/InRelease$ 0 0% 0 refresh-ims
refresh_pattern \/(Translation-.*)(|\.bz2|\.gz|\.xz)$ 0 0% 0 refresh-ims

# handle meta-release and changelogs.ubuntu.com special
# (fine to have this on debian too)
refresh_pattern changelogs.ubuntu.com\/.*  0  1% 1
# End: handling of Debian packages
