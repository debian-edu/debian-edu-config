# To use this library, add
# . /usr/share/debian-edu-config/testsuite-lib.sh
# at the top of your script.

# Check if a deb package is installed.  Return true if it is, and
# false if it isn't.
deb_installed() {
    RET=$( dpkg -s $1 2>/dev/null | awk '/Status:/ {print $4}' )
    if [ "$RET" = "installed" ] ; then
        true
    else
        false
    fi
}

# Check if something is listening on the given port and protocol
netstat_check() {
    port=$1
    proto=$2
    service="$3"

    if netstat -a --numeric-hosts 2>&1 | grep ":$port " | grep -q "^$proto" ; then
	echo "success: $0: $service service is listening on $port/$proto."
	true
    else
	echo "error: $0: $service service is not listening on $port/$proto."
	false
    fi
}

check_file_perm() {
    file=$1
    perm=$2
    if [ -e $file ] ; then
	curperm="$(stat -L -c '%a' $file)"
	if [ $perm != "$curperm" ] ; then
            echo "error: $file file mode is wrong, $curperm should be $perm"
	else
            echo "success: $file is readable by all"
	fi
    else
	echo "error: Missing $file"
    fi

}
