#!/bin/sh

# firefox-esr, libreoffice and KDE are already localized; do it for some more
# packages that are installed by default (all supported desktop environments).
#
# schweer, 2018-12-15.

set -e

. /usr/share/debian-edu-config/testsuite-lib.sh

langsfull="$(locale -a|grep utf8|cut -d'.' -f1)"
for lang in $langsfull ; do
    baselang="${lang%_*}"
    langcountry="$(echo ${lang%.*}|cut -d'_' -f2 | tr '[:upper:]' '[:lower:]')"
    for i in debian-edu-doc debian-edu-doc-legacy gimp-help thunderbird-l10n; do
        if ! [ -z "$(apt-cache search $i-$baselang-$langcountry)" ] && ! deb_installed $i-$baselang-$langcountry ; then
            apt-get -yq install $i-$baselang-$langcountry || true
        else
            if ! [ -z "$(apt-cache search $i-$baselang)" ] && \
                ! [ "thunderbird-l10n-en" = "$i-$baselang" ] && ! deb_installed $i-$baselang ; then
                # caveat: $i-$baselang might be missing, e.g. es_PE: only $i-es-ar and $i-es-es exist.
                apt-get -y install $i-$baselang || true
        fi
        fi
    done
    # special cases nn_NO and nb_NO (debian-edu-doc and gimp-help).
    if [ "nn" = "$baselang" ] && [ "no" = "$langcountry" ] && \
        ! [ -z "$(apt-cache search debian-edu-doc-nb-no)" ] && ! deb_installed debian-edu-doc-nb-no ; then
            apt-get -yq install debian-edu-doc-nb-no
    fi
    if [ "nn" = "$baselang" ] && [ "no" = "$langcountry" ] && \
        ! [ -z "$(apt-cache search debian-edu-doc-legacy-nb-no)" ] && ! deb_installed debian-edu-doc-legacy-nb-no; then
            apt-get -yq install debian-edu-doc-legacy-nb-no
    fi
    if [ "nb" = "$baselang" ] && [ "no" = "$langcountry" ] && \
        ! [ -z "$(apt-cache search gimp-help-nn)" ] && ! deb_installed install gimp-help-nn ; then
            # caveat: gimp might be missing for some archs.
            apt-get -yq install gimp-help-nn || true
    fi
done

