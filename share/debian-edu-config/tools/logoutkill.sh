#!/bin/bash

# $Id$
# This script could be run from /etc/kde2/kdm/Xreset by adding the line 
# '/usr/share/debian-edu-config/tools/logoutkill.sh $USER'
# right before the line 
# 'exit 0'


KEEPONES="root USER daemon"
USERLIMIT=10000

if [ -f /etc/debian-edu/logoutkill.conf ] ; then 
  . /etc/debian-edu/logoutkill.conf
fi 

for EVERY in $KEEPONES ; do 
  KEEP="${KEEP:+$KEEP|}^$EVERY"
done

USER=$1

if [ -z "$USER" ] ; then 
  echo -e "usage:\n\t$0 <username>"
  exit 0
fi

ps auxw | grep ^$USER | grep -vE $KEEP | \
  while read PUSER PID NULL ; do 
    [ "$PUSER" = "$USER" -a $(id -u $PUSER) -ge $USERLIMIT ] && kill $PID ; 
  done

sleep 10
  
ps auxw | grep ^$USER | grep -vE $KEEP | \
  while read PUSER PID NULL ; do 
    [ "$PUSER" = "$USER" -a $(id -u $PUSER) -ge $USERLIMIT ] && kill -9 $PID ; 
  done
