########################################################
## define the gosa configuration stuff;
## prepare 2 departments: teachers and students;
## define 4 groups: teachers, students. jradmins and nonetblk
## define templates to add a teacher or a student.
## define access roles: admin-role, and jradmin-role
## define access groups: gosa-admins, printer-admins, icinga-admins
########################################################
## gosa-config:

dn: ou=configs,ou=systems,dc=skole,dc=skolelinux,dc=no
objectClass: organizationalUnit
ou: configs

dn: ou=gosa,ou=configs,ou=systems,dc=skole,dc=skolelinux,dc=no
objectClass: organizationalUnit
ou: gosa

dn: cn=netgroupSystem,ou=gosa,ou=configs,ou=systems,dc=skole,dc=skolelinux,dc=no
cn: netgroupSystem
objectClass: top
objectClass: gosaConfig
gosaSetting: netgroupSystemRDN:ou=netgroup

###################### Teachers #########################

dn: ou=Teachers,dc=skole,dc=skolelinux,dc=no
objectClass: top
objectClass: organizationalUnit
objectClass: gosaDepartment
objectClass: labeledURIObject
description: all teachers
ou: Teachers

dn: ou=people,ou=Teachers,dc=skole,dc=skolelinux,dc=no
objectClass: organizationalUnit
ou: people

dn: ou=group,ou=Teachers,dc=skole,dc=skolelinux,dc=no
objectClass: organizationalUnit
ou: groups

###################### Students #########################
## ACLs: Teachers have jradmin-role permissions on the students department:
## echo -n cn=jradmin-role,ou=aclroles,dc=skole,dc=skolelinux,dc=no | base64 -w0
## echo -n cn=teachers,ou=group,ou=Teachers,dc=skole,dc=skolelinux,dc=no | base64 -w0
 
dn: ou=Students,dc=skole,dc=skolelinux,dc=no
objectClass: top
objectClass: gosaDepartment
objectClass: organizationalUnit
objectClass: labeledURIObject
description: all students
ou: Students
objectClass: gosaAcl
gosaAclEntry: 0:role:$JRADMINROLEDN64:$TEACHERSDN64

dn: ou=people,ou=Students,dc=skole,dc=skolelinux,dc=no
objectClass: organizationalUnit
ou: people

dn: ou=group,ou=Students,dc=skole,dc=skolelinux,dc=no
objectClass: organizationalUnit
ou: groups

###################### jradmins #########################
dn: cn=jradmins,ou=group,dc=skole,dc=skolelinux,dc=no
objectClass: top
objectClass: posixGroup
cn: jradmins
description: All junior admins in the institution
gidNumber: 10002

###################### nonetblk #########################dn:
dn: cn=nonetblk,ou=group,dc=skole,dc=skolelinux,dc=no
objectClass: top
objectClass: posixGroup
cn: nonetblk
description: Users that should be unaffected by network blocking
gidNumber: 10005


################### ACL Roles ########################
## define two ACL roles: admin-role and jradmin-role:

dn: ou=aclroles,dc=skole,dc=skolelinux,dc=no
objectClass: organizationalUnit
ou: aclroles

dn: cn=admin-role,ou=aclroles,dc=skole,dc=skolelinux,dc=no
objectClass: top
objectClass: gosaRole
gosaAclTemplate: 0:psub::all/all;cmdrw
description: nearly unlimited administrative permissions
cn: admin-role

dn: cn=jradmin-role,ou=aclroles,dc=skole,dc=skolelinux,dc=no
objectClass: top
objectClass: gosaRole
gosaAclTemplate: 0:sub::users/user;cmdrw,users/password;rw,users/posixAccount;r,groups/group;cmdr#description;w#memberUid;rw
description: limited administrative permissions
cn: jradmin-role

dn: cn=gosa-admins,ou=group,dc=skole,dc=skolelinux,dc=no
objectClass: top
objectClass: posixGroup
cn: gosa-admins
description: GOsa² Administrators
gidNumber: 60000
memberUid: $FIRSTUSERNAME

dn: cn=printer-admins,ou=group,dc=skole,dc=skolelinux,dc=no
objectClass: top
objectClass: posixGroup
cn: printer-admins
description: Printer Operators
gidNumber: 60010
memberUid: $FIRSTUSERNAME

dn: cn=icinga-admins,ou=group,dc=skole,dc=skolelinux,dc=no
objectClass: top
objectClass: posixGroup
cn: icinga-admins
description: Icinga Administrators
gidNumber: 60020
memberUid: $FIRSTUSERNAME


################### Incoming Arp Devices ##############

dn: ou=incoming,dc=skole,dc=skolelinux,dc=no
objectClass: organizationalUnit
ou: incoming


################### Templates ########################
# Groups and user templates for teachers and students

dn: cn=teachers,ou=group,ou=Teachers,dc=skole,dc=skolelinux,dc=no
objectClass: top
objectClass: posixGroup
cn: teachers
description: group of all teachers
gidNumber: 10003
memberUid: newteacher
memberUid: $FIRSTUSERNAME

## predefine template newteacher:
dn: uid=newteacher,ou=people,ou=Teachers,dc=skole,dc=skolelinux,dc=no
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
objectClass: gosaAccount
objectClass: gosaUserTemplate
objectClass: posixAccount
objectClass: shadowAccount
sn: NewTeacher
givenName: NewTeacher
uid: newteacher
cn: NewTeacher NewTeacher
userPassword: {SSHA}N0T$3T4N0W
homeDirectory: /skole/tjener/home0/%uid
loginShell: /bin/bash
uidNumber: 1001
gidNumber: 1001
gecos: NewTeacher NewTeacher
shadowLastChange: 14818

dn: cn=newteacher,ou=group,ou=Teachers,dc=skole,dc=skolelinux,dc=no
objectClass: top
objectClass: posixGroup
cn: newteacher
description: Group of user newteacher newteacher
gidNumber: 1001

dn: cn=students,ou=group,ou=Students,dc=skole,dc=skolelinux,dc=no
objectClass: top
objectClass: posixGroup
cn: students
description: group of all students
gidNumber: 10004
memberUid: newstudent
memberUid: newteacher
memberUid: $FIRSTUSERNAME

## predefine template newstudent:
dn: uid=newstudent,ou=people,ou=Students,dc=skole,dc=skolelinux,dc=no
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
objectClass: gosaAccount
objectClass: gosaUserTemplate
objectClass: posixAccount
objectClass: shadowAccount
sn: NewStudent
givenName: NewStudent
uid: newstudent
cn: NewStudent NewStudent
userPassword: {SSHA}N0T$3T4N0W
homeDirectory: /skole/tjener/home0/%uid
loginShell: /bin/bash
uidNumber: 1002
gidNumber: 1002
gecos: NewStudent NewStudent
shadowLastChange: 14818

dn: cn=newstudent,ou=group,ou=Students,dc=skole,dc=skolelinux,dc=no
objectClass: top
objectClass: posixGroup
cn: newstudent
description: Group of user newstudent newstudent
gidNumber: 1002
