bundle common edu
#
# This common bundle defines global Debian Edu vars and classes.
# If vars are used in bundles of type agent they must be prepended with 'edu',
# for example 'edu.ldapbase' to be available.
#
# Author: Wolfgang Schweer <wschweer@arcor.de>, Dec. 2017

{

  vars:

    "ldapserver"	string => execresult("/usr/bin/debian-edu-ldapserver -f", "noshell");
    "ldapbase"		string => execresult("/usr/bin/debian-edu-ldapserver -b -f", "noshell");
    "syslogserver"	string => execresult("/usr/share/debian-edu-config/tools/locate-syslog-collector", "noshell");

  classes:

    "server"		expression => returnszero("/bin/grep 'Main-Server' /etc/debian-edu/config","noshell");
    "ltspserver"	expression => fileexists("/usr/share/ltsp/ltsp");
    "workstation"	expression => returnszero("/bin/grep 'Workstation' /etc/debian-edu/config","noshell");
    "roaming"		expression => returnszero("/bin/grep 'Roaming-Workstation' /etc/debian-edu/config","noshell");
    "standalone"	expression => returnszero("/bin/grep 'Standalone' /etc/debian-edu/config","noshell");
    "minimal"		expression => returnszero("/bin/grep 'Minimal' /etc/debian-edu/config","noshell");
    "installation"	expression => fileexists("/sbin/start-stop-daemon.REAL");
    "testinstall"	expression => returnszero("/bin/grep 'TESTINSTALL=\"true\"' /etc/debian-edu/config","noshell");
    # Set if the internet is reachable for downloading files.
    "internet"		expression => returnszero("/usr/bin/wget -qO /dev/null http://deb.debian.org/debian/README","noshell");
    # Set if atomic partioning has been used (everything on one partion, w/ or w/o lvm).
    "atomic"		expression => returnszero("/usr/bin/namei -mol /skole | /bin/grep T", "useshell");
    # Set if the Squid cache exists; used to conditionally link Debian Edu configuration.
    "squidcache"	expression => isdir("/var/spool/squid");
    # Set if running inside Debian Installer; used to avoid useless LDAP setup upon main server upgrade.
    "di"			expression => fileexists("/etc/apt/apt.conf.d/00IgnoreTimeConflict");
    # Set for networked workstation if the 'education-desktop-other' package is installed.
    "desktopintern"	and => { isdir("/usr/share/doc/education-desktop-other"), isdir("/usr/share/doc/education-networked") };
    # Set if xfce4 is installed; used to conditionally configure settings.
    "xfce"		expression => isdir("/etc/xdg/xfce4");
    # Set if lxqt is installed; used to conditionally configure settings.
    "lxqt"			expression => isdir("/usr/share/pcmanfm-qt/lxqt");
    # Set if lightdm-gtk-greeter is installed; used to conditionally configure the panel.
    "lightdm"		expression => isdir("/usr/share/lightdm/lightdm-gtk-greeter.conf.d/");
    # Set if systemd-timesyncd is installed; used to conditionally configure networked clients.
    "timesyncd"		expression => fileexists("/lib/systemd/systemd-timesyncd");
}
