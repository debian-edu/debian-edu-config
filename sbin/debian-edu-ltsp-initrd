#!/bin/bash
#
# Debian Edu specific replacement for 'ltsp initrd' command.
# Author/Copyright:	Wolfgang Schweer <wschweer@arcor.de>
# Licence:		GPL2+
# first edited:	2021-11-10

version=2021-11-11

set -e

usage() {
	cat <<EOF
Usage: $0 command

Update specific LTSP initrd file (ltsp.img) for each use case. This needs to be
done after the /etc/ltsp/ltsp.conf [clients] section has been modified.

Options:
	--help     Display the debian-edu-ltsp-initrd help message.
	--version  Output version information and exit.
EOF
}

thin() {
	type="$1"
	# Add specific configuration.
	cat <<EOF >> /etc/ltsp/ltsp.conf
POST_INIT_THIN_USER='useradd -G disk -m -d /run/home/thin -k /etc/ltsp/skel -r thin'
POST_INIT_SYSTEMD='mkdir /etc/systemd/system/getty@tty1.service.d && \
cp /etc/ltsp/getty@tty1.service.d/override.conf /etc/systemd/system/getty@tty1.service.d'
POST_INIT_AUTOFS='cp /etc/ltsp/autofs/extra.autofs /etc/auto.master.d && \
cp /etc/ltsp/autofs/auto.* /etc'
EOF
	# Create the ltsp.img file and move it to the right location.
	echo "Updating LTSP initrd for $type:"
	ltsp initrd
	mv /srv/tftp/ltsp/ltsp.img /srv/tftp/ltsp/"$type"/ltsp.img
	echo "Moved /srv/tftp/ltsp/ltsp.img to /srv/tftp/ltsp/$type/"

	# Clean up ltsp.conf from image specific items.
	sed -i '/POST_INIT/d' /etc/ltsp/ltsp.conf
}

dlw() {
	# Add specific configuration.
	cat <<EOF >> /etc/ltsp/ltsp.conf
LIGHTDM_CONF="greeter-hide-users"
POST_INIT_USE_FQDN="sed -i '/10.0.2.2/ s/server/tjener.intern tjener/' /etc/hosts"
EOF
	# Create the ltsp.img file and move it to the right location.
	echo "Updating LTSP initrd for dlw:"
	ltsp initrd
	mv /srv/tftp/ltsp/ltsp.img /srv/tftp/ltsp/dlw/
	echo "Moved /srv/tftp/ltsp/ltsp.img to /srv/tftp/ltsp/dlw/"

	# Clean up ltsp.conf from image specific items.
	sed -i '/POST_INIT/d' /etc/ltsp/ltsp.conf
	sed -i '/LIGHTDM_CONF/d' /etc/ltsp/ltsp.conf
}

server() {

	# Add specific configuration.
	if echo "$PROFILE" | grep -Eq 'Main-Server' ; then
		# The image is a copy of the main server's fs. On the server, autofs
		# is disabled, but it is needed for diskless workstations.
		# OTOH some services need to be disabled, i.e. 'masked'.
		cat <<EOF >> /etc/ltsp/ltsp.conf
PRE_INIT_AUTOFS="echo 'LDAPURI=ldap://ldap' >> /etc/default/autofs"
PRE_INIT_MAIN_SERVER="systemctl enable autofs"
POST_INIT_USE_FQDN="sed -i '/10.0.2.2/ s/server/tjener.intern tjener/' /etc/hosts"
POST_INIT_SITESUMMARY="sed -i 's/main-server/ltsp-client/' /etc/sitesummary/hostclass"
MASK_SYSTEM_SERVICES="apache2 named cups dovecot etckeeper exim4 squid tftpd-hpa \
icinga2 nmbd slapd smbd systemd-journald xrdp xrdp-sesman krb5-kdc mariadb cfengine3 isc-dhcp-server"
EOF
	else
		cat <<EOF >> /etc/ltsp/ltsp.conf
MASK_SYSTEM_SERVICES="etckeeper exim4 tftpd-hpa isc-dhcp-server xrdp xrdp-sesman"
EOF
	fi
	# Update the ltsp.img file and move it to the right location.
	echo "Updating LTSP initrd for use with the server's filesystem:"
	ltsp initrd
	if uname -r | grep -q 686 ; then
		mv /srv/tftp/ltsp/ltsp.img /srv/tftp/ltsp/x86_32/ltsp.img
		echo "Moved /srv/tftp/ltsp/ltsp.img to /srv/tftp/ltsp/x86_32/"
	else
		mv /srv/tftp/ltsp/ltsp.img /srv/tftp/ltsp/"$(uname -m)"/ltsp.img
		echo "Moved /srv/tftp/ltsp/ltsp.img to /srv/tftp/ltsp/x86_64/"
	fi

	# Clean up ltsp.conf from specific items.
	sed -i '/PRE_INIT_MAIN/d' /etc/ltsp/ltsp.conf
	sed -i '/PRE_INIT_AUTOFS/d' /etc/ltsp/ltsp.conf
	sed -i '/MASK_SYSTEM/d' /etc/ltsp/ltsp.conf
}

case "$1" in
	-h|--help) usage ; exit 0 ;;
	--version) echo $version ; exit 0 ;;
esac

# Make Debian Edu profile available.
if [ -r /etc/debian-edu/config ] ; then
	. /etc/debian-edu/config
fi

# LTSP images base directory
BASE="/srv/tftp/ltsp"

if [ -d "$BASE"/x86_64 ] || [ -d "$BASE"/x86_32 ]; then server ; fi
if [ -d "$BASE"/dlw ] ; then dlw ; fi

# X2Go thin client variants.
variants="bare-amd64 display-amd64 desktop-amd64 bare-i386 display-i386 desktop-i386"

for i in $variants ; do
    if [ -d $BASE/x2go-"$i" ] ; then thin x2go-"$i" ; fi
done

case "$1" in
	-h|--help) usage ; exit 0 ;;
	--version) echo $version ; exit 0 ;;
	*) exit 0 ;;
esac
