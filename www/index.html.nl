<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="nl">
  <head>
    <title>Welkom op &quot;www&quot;: Informatiepagina van een Debian Edu installatie</title>
    <link rev="made" href="mailto:linuxiskolen@skolelinux.no"/>
    <link rel="top" href="https://wiki.debian.org/DebianEdu"/>
    <link rel="alternate" title="norsk" href="index.html.nb-no"/>
    <link rel="alternate" title="English" href="index.html.en"/>
    <link rel="alternate" title="dansk" href="index.html.da"/>
    <link rel="alternate" title="Deutsch" href="index.html.de"/>
    <link rel="alternate" title="français" href="index.html.fr"/>
    <link rel="alternate" title="català" href="index.html.ca"/>
    <link rel="alternate" title="español" href="index.html.es-es"/>
    <link rel="alternate" title="Indonesia" href="index.html.id"/>
    <link rel="alternate" title="Italiano" href="index.html.it"/>
    <link rel="alternate" title="日本語" href="index.html.ja"/>
    <link rel="alternate" title="Nederlands" href="index.html.nl"/>
    <link rel="alternate" title="Português" href="index.html.pt-pt"/>
    <link rel="alternate" title="Português do Brasil" href="index.html.pt-br"/>
    <link rel="alternate" title="Română" href="index.html.ro"/>
    <link rel="alternate" title="Русский" href="index.html.ru"/>
    <link rel="alternate" title="中文" href="index.html.zh-tw"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="Language" content="nl"/>
    <meta name="Author" content="Ole Aamot, Petter Reinholdtsen, Gaute Hvoslef Kvalnes, Frode Jemtland"/>
    <link rel="stylesheet" href="skl-ren_css.css" type="text/css"/>
  </head>

  <body>

    <div class="hbar"/>

    <div class="menu">

      <h2 class="menu">Lokale netwerkdiensten</h2>
     <ul>
      <li><a href="/debian-edu-doc/nl/">Documentatie</a></li>
      <li><a href="/gosa/">GOsa<sup>2</sup> LDAP-beheer</a></li>
      <li><a href="https://www.intern:631/">Printerbeheer</a></li>
      <li><a href="/slbackup-php">Backup</a></li>
      <li><a href="/icingaweb2/">Icinga</a></li>
      <li><a href="/munin/">Munin</a></li>
      <li><a href="/sitesummary/">Sitesummary</a></li>
     </ul>

      <h2 class="menu">Debian Edu</h2>
     <ul>
      <li><a href="https://blends.debian.org/edu/">Webpagina</a></li>
      <li><a href="https://wiki.debian.org/DebianEdu">Wikipagina</a></li>
      <li><a href="https://wiki.debian.org/DebianEdu/MailingLists">E-maillijsten</a></li>
      <li><a href="https://popcon.debian.org/">Statistieken over pakketgebruik</a></li>
     </ul>

    </div>

    <div class="body">

    <div align="left">

      <!-- Note to translators: these strings should not be translated -->
<a href="index.html.ca">[català]</a> <a href="index.html.da">[dansk]</a> <a
href="index.html.de">[Deutsch]</a> <a href="index.html.en">[English]</a> <a
href="index.html.es-es">[español]</a> <a href="index.html.fr">[français]</a>
<a href="index.html.id">[Indonesia]</a> <a
href="index.html.it">[Italiano]</a> <a href="index.html.nb-no">[norsk]</a>
<a href="index.html.nl">[Nederlands]</a> <a
href="index.html.pt-pt">[Português]</a> <a
href="index.html.pt-br">[Português do Brasil]</a> <a
href="index.html.ro">[Română]</a> <a href="index.html.ru">[Русский]</a> <a
href="index.html.zh-tw">[中文]</a> <a href="index.html.ja">[日本語]</a>
    </div>


    <h1><a name="top"><img src="logo-trans.png" alt="Skolelinux"/></a></h1>

    <h2>Welkom bij Debian-Edu / Skolelinux</h2>
    <p><strong>Wanneer u dit leest kunt u ervan uit gaan dat de installatie van uw
Debian-Edu server gelukt is. Proficiat en welkom. U kunt de inhoud van deze
pagina wijzigen door het bestand /etc/debian-edu/www/index.html.nl aan te
passen in uw favoriete teksteditor.</strong></p>
    <p>
    Aan de rechterkant van deze pagina ziet u enkele links die nuttig kunnen
zijn tijdens het beheer van het Debian-Edu netwerk.
    </p>
    <ul>
     <li>De links onder 'Lokale Netwerkdiensten' zijn links naar
administratiepagina's van diensten die deze server aanbiedt. Deze
hulpmiddelen kunnen u helpen in het dagelijks beheer van uw Debian-Edu
netwerk.
    </li>
     <li>De links onder Debian-Edu zijn links naar Debian-Edu en/of Skolelinux
pagina's op het Internet.
     <ul>
      <li><strong>Documentatie:</strong> Hier kunt u de beschikbare documentatie
lezen.</li>
      <li><strong>GOsa<sup>2</sup> LDAP-beheer:</strong> Kies dit om naar het
GOsa<sup>2</sup> LDAP-beheer-websysteem te gaan. Hier kunt u gebruikers en
machines toevoegen en aanpassen.</li>
      <li><strong>Printerbeheer:</strong> Kies dit om printerinstellingen te beheren.</li>
      <li><strong>Backup:</strong> Het systeem voor backups, waar u de nachtelijke
reservekopie kunt terugzetten of wijzigen.</li>
      <li><strong>Icinga:</strong> Dit zijn de Icinga systeemmonitor-pagina's.</li>
      <li><strong>Munin:</strong> Dit leidt naar de Munin-pagina's met statistieken.</li>
      <li><strong>Sitesummary:</strong> Kies dit om een overzicht te krijgen van de
Debian-Edu netwerkmachines.</li> 
     </ul>
     </li>
    </ul>
    <h2>Eigen webpagina's voor gebruikers op het systeem.</h2>
    <p>Alle gebruikers op het systeem kunnen een eigen webpagina aanmaken. Hiertoe
dient enkel een submap &quot;public_html&quot; aangemaakt te worden in de
thuismap van de gebruiker.  Webpagina's die in deze map geplaatst worden
zijn dan beschikbaar op het adres https://www/~gebruikersnaam/. Als u dus
een gebruiker 'Jan Klaasen' heeft met gebruikersnaam 'jank', zijn door hem
gemaakte webpagina's dus te vinden op <a
href="https://www/~jank/">https://www/~jank/</a>. Wanneer de gebruiker of de
submap public_html niet bestaan krijgt u de &quot;Not Found&quot;
foutpagina.  Wanneer de submap public_html wel bestaat maar leeg is, krijgt
u de &quot;Permission Denied&quot; foutpagina te zien. Om dit te voorkomen
dient u een index.html bestand aan te maken in de public_html map.</p>

    </div>
  </body>
</html>
