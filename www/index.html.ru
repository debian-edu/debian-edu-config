<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="ru">
  <head>
    <title>Добро пожаловать на &laquo;веб-страницу&raquo; информации об установке
Debian Edu</title>
    <link rev="made" href="mailto:linuxiskolen@skolelinux.no"/>
    <link rel="top" href="https://wiki.debian.org/DebianEdu"/>
    <link rel="alternate" title="norsk" href="index.html.nb-no"/>
    <link rel="alternate" title="English" href="index.html.en"/>
    <link rel="alternate" title="dansk" href="index.html.da"/>
    <link rel="alternate" title="Deutsch" href="index.html.de"/>
    <link rel="alternate" title="français" href="index.html.fr"/>
    <link rel="alternate" title="català" href="index.html.ca"/>
    <link rel="alternate" title="español" href="index.html.es-es"/>
    <link rel="alternate" title="Indonesia" href="index.html.id"/>
    <link rel="alternate" title="Italiano" href="index.html.it"/>
    <link rel="alternate" title="日本語" href="index.html.ja"/>
    <link rel="alternate" title="Nederlands" href="index.html.nl"/>
    <link rel="alternate" title="Português" href="index.html.pt-pt"/>
    <link rel="alternate" title="Português do Brasil" href="index.html.pt-br"/>
    <link rel="alternate" title="Română" href="index.html.ro"/>
    <link rel="alternate" title="Русский" href="index.html.ru"/>
    <link rel="alternate" title="中文" href="index.html.zh-tw"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="Language" content="ru"/>
    <meta name="Author" content="Ole Aamot, Petter Reinholdtsen, Gaute Hvoslef Kvalnes, Frode Jemtland"/>
    <link rel="stylesheet" href="skl-ren_css.css" type="text/css"/>
  </head>

  <body>

    <div class="hbar"/>

    <div class="menu">

      <h2 class="menu">Локальные сервисы</h2>
     <ul>
      <li><a href="/debian-edu-doc/en/">Документация</a></li>
      <li><a href="/gosa/">Управление LDAP(GOsa<sup>2</sup>)</a></li>
      <li><a href="https://www.intern:631/">Управление принтерами</a></li>
      <li><a href="/slbackup-php">Резервное копирование</a></li>
      <li><a href="/icingaweb2/">Icinga</a></li>
      <li><a href="/munin/">Munin</a></li>
      <li><a href="/sitesummary/">О площадке</a></li>
     </ul>

      <h2 class="menu">Debian Edu</h2>
     <ul>
      <li><a href="https://blends.debian.org/edu/">Веб-страница</a></li>
      <li><a href="https://wiki.debian.org/DebianEdu">Вики</a></li>
      <li><a href="https://wiki.debian.org/DebianEdu/MailingLists">Списки рассылки</a></li>
      <li><a href="https://popcon.debian.org/">Статистика использования пакетов</a></li>
     </ul>

    </div>

    <div class="body">

    <div align="left">

      <!-- Note to translators: these strings should not be translated -->
<a href="index.html.ca">[català]</a> <a href="index.html.da">[dansk]</a> <a
href="index.html.de">[Deutsch]</a> <a href="index.html.en">[English]</a> <a
href="index.html.es-es">[español]</a> <a href="index.html.fr">[français]</a>
<a href="index.html.id">[Indonesia]</a> <a
href="index.html.it">[Italiano]</a> <a href="index.html.nb-no">[norsk]</a>
<a href="index.html.nl">[Nederlands]</a> <a
href="index.html.pt-pt">[Português]</a> <a
href="index.html.pt-br">[Português do Brasil]</a> <a
href="index.html.ro">[Română]</a> <a href="index.html.ru">[Русский]</a> <a
href="index.html.zh-tw">[中文]</a> <a href="index.html.ja">[日本語]</a>
    </div>


    <h1><a name="top"><img src="logo-trans.png" alt="Skolelinux"/></a></h1>

    <h2>Добро пожаловать в Debian Edu / Skolelinux</h2>
    <p><strong>Если вы видите этот текст, то это значит, что установка вашего
сервера Debian Edu успешно завершена. Поздравляем! Чтобы изменить содержимое
этой страницы отредактируйте файл /etc/debian-edu/www/index.html.ru своим
любимым редактором.</strong></p>
    <p>
    В правой части этой страницы есть несколько ссылок, которые помогут вам
управлять сетью Debian Edu.
    </p>
    <ul>
     <li>Ссылки ниже Локальных сервисов указывают на сервисы, запущенные на этом
сервере. Эти инструменты помогут вам при каждодневной работе с Debian Edu.
    </li>
     <li>Ссылки ниже Debian Edu указывают на страницы Debian Edu и/или Skolelinux в
интернете.
     <ul>
      <li><strong>Документация:</strong> Перейдите по ссылке, чтобы посмотреть
установленную документацию.</li>
      <li><strong>Управление LDAP (GOsa<sup>2</sup>):</strong> Перейдя по ссылке, вы
попадёте в веб-интерфейс управления LDAP GOsa<sup>2</sup>. Используйте его
для добавления/изменения пользователей и машин.</li>
      <li><strong>Управление принтерами:</strong> Перейдите по ссылке, если хотите
настроить принтеры.</li>
      <li><strong>Резервное копирование:</strong> Перейдите по ссылке, чтобы попасть в
систему резервного копирования. Там вы сможете восстановить данные или
изменить ночное резервное копирование.</li>
      <li><strong>Icinga:</strong> Перейдя по ссылке, вы попадёте на страницы системы
мониторинга Icinga.</li>
      <li><strong>Munin:</strong> Перейдя по ссылке, вы попадёте на страницы
статистики Munin.</li>
      <li><strong>О площадке:</strong> Перейдя по ссылке, вы попадёте на страницы
статистики по машинам в сети Debian Edu.</li> 
     </ul>
     </li>
    </ul>
    <h2>Страницы пользователей системы.</h2>
    <p>Все пользователи системы могут создать каталог в своём домашнем каталоге с
именем &laquo;public_html&raquo;. Туда пользователи могут добавлять свои
собственные домашние страницы. Домашние страницы будут доступны по адресу
https://www/~имя_пользователя/. Например, если пользователя зовут Jon Doe и
имя его учётной записи jond, то его веб-страница будет доступна по адресу <a
href="https://www/~jond/">https://www/~jond/</a>. Если пользователь не
существует или не существует каталог public_html, то вы увидите страницу
ошибки &laquo;Not Found&raquo;. Если каталог public_html существует, но он
пустой, то вы увидите страницу ошибки &laquo;Permission Denied&raquo;. Чтобы
это не случалось, создайте файл index.html в каталоге public_html.</p>

    </div>
  </body>
</html>
