<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <title>Welcome to &laquo;www&raquo;: Info page for a Debian Edu installation</title>
    <link rev="made" href="mailto:linuxiskolen@skolelinux.no"/>
    <link rel="top" href="https://wiki.debian.org/DebianEdu"/>
    <link rel="alternate" title="norsk" href="index.html.nb-no"/>
    <link rel="alternate" title="English" href="index.html.en"/>
    <link rel="alternate" title="dansk" href="index.html.da"/>
    <link rel="alternate" title="Deutsch" href="index.html.de"/>
    <link rel="alternate" title="français" href="index.html.fr"/>
    <link rel="alternate" title="català" href="index.html.ca"/>
    <link rel="alternate" title="español" href="index.html.es-es"/>
    <link rel="alternate" title="Indonesia" href="index.html.id"/>
    <link rel="alternate" title="Italiano" href="index.html.it"/>
    <link rel="alternate" title="日本語" href="index.html.ja"/>
    <link rel="alternate" title="Nederlands" href="index.html.nl"/>
    <link rel="alternate" title="Português" href="index.html.pt-pt"/>
    <link rel="alternate" title="Português do Brasil" href="index.html.pt-br"/>
    <link rel="alternate" title="Română" href="index.html.ro"/>
    <link rel="alternate" title="Русский" href="index.html.ru"/>
    <link rel="alternate" title="中文" href="index.html.zh-tw"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="Language" content="en"/>
    <meta name="Author" content="Ole Aamot, Petter Reinholdtsen, Gaute Hvoslef Kvalnes, Frode Jemtland"/>
    <link rel="stylesheet" href="skl-ren_css.css" type="text/css"/>
  </head>

  <body>

    <div class="hbar"/>

    <div class="menu">

      <h2 class="menu">Local services</h2>
     <ul>
      <li><a href="/debian-edu-doc/en/">Documentation</a></li>
      <li><a href="/gosa/">GOsa<sup>2</sup> LDAP administration</a></li>
      <li><a href="https://www.intern:631/">Printer administration</a></li>
      <li><a href="/slbackup-php">Backup</a></li>
      <li><a href="/icingaweb2/">Icinga</a></li>
      <li><a href="/munin/">Munin</a></li>
      <li><a href="/sitesummary/">Sitesummary</a></li>
     </ul>

      <h2 class="menu">Debian Edu</h2>
     <ul>
      <li><a href="https://blends.debian.org/edu">Web page</a></li>
      <li><a href="https://wiki.debian.org/DebianEdu">Wiki page</a></li>
      <li><a href="https://wiki.debian.org/DebianEdu/MailingLists">Email lists</a></li>
      <li><a href="https://popcon.debian.org/">Collected package usage</a></li>
     </ul>

    </div>

    <div class="body">

    <div align="left">
<!-- Note to translators: these strings should not be translated -->
      <a href="index.html.ca">[català]</a>
      <a href="index.html.da">[dansk]</a>
      <a href="index.html.de">[Deutsch]</a>
      <a href="index.html.en">[English]</a>
      <a href="index.html.es-es">[español]</a>
      <a href="index.html.fr">[français]</a>
      <a href="index.html.id">[Indonesia]</a>
      <a href="index.html.it">[Italiano]</a>
      <a href="index.html.nb-no">[norsk]</a>
      <a href="index.html.nl">[Nederlands]</a> 
      <a href="index.html.pt-pt">[Português]</a>
      <a href="index.html.pt-br">[Português do Brasil]</a>
      <a href="index.html.ro">[Română]</a>
      <a href="index.html.ru">[Русский]</a> 
      <a href="index.html.zh-tw">[中文]</a> 
      <a href="index.html.ja">[日本語]</a>
    </div>


    <h1><a name="top"><img src="logo-trans.png" alt="Skolelinux"/></a></h1>

    <h2>Welcome to Debian Edu / Skolelinux</h2>
    <p><strong>If you can see this, it means the installation of your 
    Debian Edu server was successful. Congratulations, and welcome. To change
    the content of this page, edit
    /etc/debian-edu/www/index.html.en, in your favorite editor.</strong></p>
    <p>
    On the right side for this page you see some links that can be helpful for you in your
    work, administrating a Debian Edu network.
    </p>
    <ul>
     <li>The links under Local services are links to services running on this server.
     These tools can assist you in your daily work with the Debian Edu solution.
    </li>
     <li>The links under Debian Edu are links to the Debian Edu and/or Skolelinux pages on the Internet.
     <ul>
      <li><strong>Documentation:</strong> Choose this to browse the installed documentation</li>
      <li><strong>GOsa<sup>2</sup> LDAP administration:</strong> Choose this to get to the GOsa<sup>2</sup> LDAP administration web system.  Use this to add and edit users and machines.</li>
      <li><strong>Printer administration:</strong> Choose this to administer your printers.</li>
      <li><strong>Backup:</strong> Choose this to get to the backup system, here you can restore or change the nightly backup</li>
      <li><strong>Icinga:</strong> Choose this to get to the Icinga system monitor pages.</li>
      <li><strong>Munin:</strong> Choose this to get to the Munin statistic pages.</li>
      <li><strong>Sitesummary:</strong> Choose this to get to the summary report of Debian Edu network machines.</li> 
     </ul>
     </li>
    </ul>
    <h2>Personal web pages for the users on the system.</h2>
    <p>All users on the system, can create a catalog on their home directory with the name 
    &laquo;public_html&raquo;. Here the users can add their own home pages. The home pages
    will be available at the address https://www/~username/. If you have a user named
    Jon Doe, with the user name jond, his web pages will be available at 
    <a href="https://www/~jond/">https://www/~jond/</a>. If the user doesn't 
    exist, or the catalog public_html do not exist, you will get the &laquo;Not Found&raquo; 
    error page. If the public_html catalog exists, but it is empty, you will get the 
    &laquo;Permission Denied&raquo; error page. To change this, create the index.html file inside
    the public_html catalog.</p>

    </div>
  </body>
</html>
